import firebase from 'firebase'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAt13E3Ch8WMOV37-jL6jfyFHnVuNbDrUU",
  authDomain: "examen-35716.firebaseapp.com",
  projectId: "examen-35716",
  storageBucket: "examen-35716.appspot.com",
  messagingSenderId: "576094801437",
  appId: "1:576094801437:web:911a3070d8e7a45ac4c0f4",
  measurementId: "G-9E2CLC27ZW"
};

  firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();

export { auth, db} 
